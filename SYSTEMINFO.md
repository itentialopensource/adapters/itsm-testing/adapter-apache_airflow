# Apache Airflow

Vendor: Apache
Homepage: https://apache.org/

Product: Airflow
Product Page: https://airflow.apache.org/

## Introduction
We classify Apache Airflow into the ITSM (Service Management) domain as Apache Airflow provides ticketing solutions for Worflow Management.

"Airflow™ is a platform created by the community to programmatically author, schedule and monitor workflows." 

## Why Integrate
The Apache Airflow adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Apache Airflow. With this adapter you have the ability to perform operations such as:

- Get DAG
- Create DAG Run
- Delete DAG Run
- Get DAG Run

## Additional Product Documentation
The [API documents for Apache Airflow](https://airflow.apache.org/docs/apache-airflow/stable/stable-rest-api-ref.html)
The [Authentication for Apache Airflow](https://airflow.apache.org/docs/apache-airflow-providers-fab/stable/auth-manager/api-authentication.html)
