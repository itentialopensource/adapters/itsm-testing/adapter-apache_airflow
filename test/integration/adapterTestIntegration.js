/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-apache_airflow',
      type: 'ApacheAirflow',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const ApacheAirflow = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Apache_airflow Adapter Test', () => {
  describe('ApacheAirflow Class Tests', () => {
    const a = new ApacheAirflow(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getAirflowConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAirflowConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Config', 'getAirflowConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConnection((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.connection_id);
                assert.equal('string', data.response.conn_type);
                assert.equal('string', data.response.host);
                assert.equal('string', data.response.login);
                assert.equal('string', data.response.schema);
                assert.equal(6, data.response.port);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.extra);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Connections', 'postConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.testConnection((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.status);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Connections', 'testConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnections((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.connections));
                assert.equal(5, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Connections', 'getConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchConnection((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Connections', 'patchConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getConnection((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.connection_id);
                assert.equal('string', data.response.conn_type);
                assert.equal('string', data.response.host);
                assert.equal('string', data.response.login);
                assert.equal('string', data.response.schema);
                assert.equal(5, data.response.port);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.extra);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Connections', 'getConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postClearTaskInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postClearTaskInstances((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.task_instances));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'postClearTaskInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDagRun - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDagRun((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dag_run_id);
                assert.equal('string', data.response.dag_id);
                assert.equal('string', data.response.logical_date);
                assert.equal('string', data.response.execution_date);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.end_date);
                assert.equal('queued', data.response.state);
                assert.equal(true, data.response.external_trigger);
                assert.equal('object', typeof data.response.conf);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'postDagRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSetTaskInstancesState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSetTaskInstancesState((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.task_instances));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'postSetTaskInstancesState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDagRunsBatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDagRunsBatch((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.dag_runs));
                assert.equal(9, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getDagRunsBatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskInstancesBatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTaskInstancesBatch((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.task_instances));
                assert.equal(9, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getTaskInstancesBatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDags((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.dags));
                assert.equal(9, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getDags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchDag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDag((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'patchDag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDag - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDag((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dag_id);
                assert.equal('string', data.response.root_dag_id);
                assert.equal(false, data.response.is_paused);
                assert.equal(false, data.response.is_active);
                assert.equal(true, data.response.is_subdag);
                assert.equal('string', data.response.fileloc);
                assert.equal('string', data.response.file_token);
                assert.equal(true, Array.isArray(data.response.owners));
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getDag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDagRuns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDagRuns((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.dag_runs));
                assert.equal(2, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getDagRuns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDagRunState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateDagRunState((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'updateDagRunState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDagRun - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDagRun((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dag_run_id);
                assert.equal('string', data.response.dag_id);
                assert.equal('string', data.response.logical_date);
                assert.equal('string', data.response.execution_date);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.end_date);
                assert.equal('queued', data.response.state);
                assert.equal(true, data.response.external_trigger);
                assert.equal('object', typeof data.response.conf);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getDagRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTaskInstances((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.task_instances));
                assert.equal(1, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getTaskInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTaskInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTaskInstance((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.task_id);
                assert.equal('string', data.response.dag_id);
                assert.equal('string', data.response.execution_date);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.end_date);
                assert.equal(10, data.response.duration);
                assert.equal('success', data.response.state);
                assert.equal(2, data.response.try_number);
                assert.equal(10, data.response.max_tries);
                assert.equal('string', data.response.hostname);
                assert.equal('string', data.response.unixname);
                assert.equal('string', data.response.pool);
                assert.equal(9, data.response.pool_slots);
                assert.equal('string', data.response.queue);
                assert.equal(4, data.response.priority_weight);
                assert.equal('string', data.response.operator);
                assert.equal('string', data.response.queued_when);
                assert.equal(4, data.response.pid);
                assert.equal('string', data.response.executor_config);
                assert.equal('object', typeof data.response.sla_miss);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getTaskInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtraLinks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtraLinks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.extra_links));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getExtraLinks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLog((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.continuation_token);
                assert.equal('string', data.response.content);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getLog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getXcomEntries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getXcomEntries((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.xcom_entries));
                assert.equal(4, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getXcomEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getXcomEntry - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getXcomEntry((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.timestamp);
                assert.equal('string', data.response.execution_date);
                assert.equal('string', data.response.task_id);
                assert.equal('string', data.response.dag_id);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getXcomEntry', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDagDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDagDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dag_id);
                assert.equal('string', data.response.root_dag_id);
                assert.equal(false, data.response.is_paused);
                assert.equal(true, data.response.is_active);
                assert.equal(false, data.response.is_subdag);
                assert.equal('string', data.response.fileloc);
                assert.equal('string', data.response.file_token);
                assert.equal(true, Array.isArray(data.response.owners));
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.timezone);
                assert.equal(false, data.response.catchup);
                assert.equal('string', data.response.orientation);
                assert.equal(5, data.response.concurrency);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.doc_md);
                assert.equal('string', data.response.default_view);
                assert.equal('object', typeof data.response.params);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getDagDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTasks((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.tasks));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getTasks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTask - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTask((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.class_ref);
                assert.equal('string', data.response.task_id);
                assert.equal('string', data.response.owner);
                assert.equal('string', data.response.start_date);
                assert.equal('string', data.response.end_date);
                assert.equal('all_success', data.response.trigger_rule);
                assert.equal(true, Array.isArray(data.response.extra_links));
                assert.equal(false, data.response.depends_on_past);
                assert.equal(true, data.response.wait_for_downstream);
                assert.equal(3, data.response.retries);
                assert.equal('string', data.response.queue);
                assert.equal('string', data.response.pool);
                assert.equal(2, data.response.pool_slots);
                assert.equal(false, data.response.retry_exponential_backoff);
                assert.equal(4, data.response.priority_weight);
                assert.equal('downstream', data.response.weight_rule);
                assert.equal('string', data.response.ui_color);
                assert.equal('string', data.response.ui_fgcolor);
                assert.equal(true, Array.isArray(data.response.template_fields));
                assert.equal('object', typeof data.response.sub_dag);
                assert.equal(true, Array.isArray(data.response.downstream_task_ids));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'getTask', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDagSource - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDagSource((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.content);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DagSources', 'getDagSource', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEventLogs((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.event_logs));
                assert.equal(6, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventLogs', 'getEventLogs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEventLog - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEventLog((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.event_log_id);
                assert.equal('string', data.response.when);
                assert.equal('string', data.response.dag_id);
                assert.equal('string', data.response.task_id);
                assert.equal('string', data.response.event);
                assert.equal('string', data.response.execution_date);
                assert.equal('string', data.response.owner);
                assert.equal('string', data.response.extra);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('EventLogs', 'getEventLog', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImportErrors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImportErrors((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.import_errors));
                assert.equal(10, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImportErrors', 'getImportErrors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImportError - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImportError((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.import_error_id);
                assert.equal('string', data.response.timestamp);
                assert.equal('string', data.response.filename);
                assert.equal('string', data.response.stack_trace);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ImportErrors', 'getImportError', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHealth((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.metadatabase);
                assert.equal('object', typeof data.response.scheduler);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'getHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVersion((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.git_version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Version', 'getVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postPool((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.slots);
                assert.equal(3, data.response.occupied_slots);
                assert.equal(5, data.response.used_slots);
                assert.equal(10, data.response.queued_slots);
                assert.equal(6, data.response.open_slots);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pools', 'postPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPools - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPools((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.pools));
                assert.equal(7, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pools', 'getPools', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchPool((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pools', 'patchPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPool((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.slots);
                assert.equal(8, data.response.occupied_slots);
                assert.equal(2, data.response.used_slots);
                assert.equal(8, data.response.queued_slots);
                assert.equal(8, data.response.open_slots);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pools', 'getPool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProviders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProviders((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.providers));
                assert.equal(4, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Providers', 'getProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVariables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVariables((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Variables', 'postVariables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVariables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVariables((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.variables));
                assert.equal(2, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Variables', 'getVariables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchVariable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchVariable((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Variables', 'patchVariable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVariable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVariable((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Variables', 'getVariable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlugins - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPlugins((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.plugins));
                assert.equal(3, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Plugins', 'getPlugins', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRole((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.actions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'postRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(4, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'getRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchRole((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'patchRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRole - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRole((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.actions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'getRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPermissions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.actions));
                assert.equal(10, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Permissions', 'getPermissions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUser((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.first_name);
                assert.equal('string', data.response.last_name);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.email);
                assert.equal(false, data.response.active);
                assert.equal('string', data.response.last_login);
                assert.equal(8, data.response.login_count);
                assert.equal(10, data.response.failed_login_count);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.changed_on);
                assert.equal('string', data.response.password);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.users));
                assert.equal(1, data.response.total_entries);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchUser((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'patchUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUser((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.first_name);
                assert.equal('string', data.response.last_name);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.email);
                assert.equal(false, data.response.active);
                assert.equal('string', data.response.last_login);
                assert.equal(7, data.response.login_count);
                assert.equal(8, data.response.failed_login_count);
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal('string', data.response.created_on);
                assert.equal('string', data.response.changed_on);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteConnection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteConnection((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Connections', 'deleteConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDag((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'deleteDag', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDagRun - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDagRun((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dags', 'deleteDagRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePool - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePool((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Pools', 'deletePool', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVariable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVariable((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Variables', 'deleteVariable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRole((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'deleteRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUser((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-apache_airflow-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
