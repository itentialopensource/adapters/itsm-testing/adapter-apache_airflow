
## 0.2.0 [05-25-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/itsm-testing/adapter-apache_airflow!1

---

## 0.1.1 [10-23-2021]

- Initial Commit

See commit 58dcf5c

---
