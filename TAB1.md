# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Apache_airflow System. The API that was used to build the adapter for Apache_airflow is usually available in the report directory of this adapter. The adapter utilizes the Apache_airflow API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Apache Airflow adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Apache Airflow. With this adapter you have the ability to perform operations such as:

- Get DAG
- Create DAG Run
- Delete DAG Run
- Get DAG Run

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
